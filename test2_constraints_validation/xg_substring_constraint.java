package test2_constraints_validation;

import javax.validation.*;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Giovanni on 29/04/2015.
 */

@Constraint(validatedBy = {xg_substring_validator.class})
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
public @interface xg_substring_constraint {

    String message() default "This string doesn't contain xg";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
