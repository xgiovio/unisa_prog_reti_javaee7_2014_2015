package test2_constraints_validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Giovanni on 29/04/2015.
 */

@Constraint(validatedBy = {general_class_validator.class})
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER,TYPE})
@Retention(RUNTIME)
public @interface general_class_constraint {

    String message() default "This is a sample of constraint for a class";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
