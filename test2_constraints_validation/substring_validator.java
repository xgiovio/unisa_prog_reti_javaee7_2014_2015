package test2_constraints_validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Giovanni on 29/04/2015.
 */
public class substring_validator implements ConstraintValidator<substring_constraint,String> {

    String xg_attribute_1;


    @Override
    public void initialize(substring_constraint substring_constraint) {
        this.xg_attribute_1 = substring_constraint.xg_attribute_1();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s.contains(this.xg_attribute_1);
    }
}
