package test2_constraints_validation;

import javax.annotation.PostConstruct;
import javax.validation.constraints.*;

/**
 * Created by Giovanni on 29/04/2015.
 */

@general_class_constraint
public class general_class {


        private String title;

        private Double price;

                @PostConstruct private void ps (){
                        title = "AAA";
                        price = 121.0;
                }

        public Boolean CheckStringLength ( String a) {
                return a.length() > 0;
        }

        public String getTitle() {
                return title;
        }

        public void setTitle(String title) {
                this.title = title;
        }

        public Double getPrice() {
                return price;
        }

        public void setPrice(Double price) {
                this.price = price;
        }
}
