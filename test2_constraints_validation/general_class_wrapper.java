package test2_constraints_validation;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by Giovanni on 29/04/2015.
 */
public class general_class_wrapper {

    @Inject @general_class_constraint
    private general_class g;

    @PostConstruct
    private void ps (){
        g.setPrice(10.0);
        g.setTitle("Artur");
    }
}
