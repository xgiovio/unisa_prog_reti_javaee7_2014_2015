package test2_constraints_validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Giovanni on 29/04/2015.
 */
public class general_class_validator implements ConstraintValidator<general_class_constraint,general_class> {



    @Override
    public void initialize(general_class_constraint general_class_constraint) {
    }

    @Override
    public boolean isValid(general_class general_class, ConstraintValidatorContext constraintValidatorContext) {
        if (general_class.getPrice() > 0 && general_class.getTitle().length() > 0)
            return true;
        return false;
    }
}
