package test2_constraints_validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Giovanni on 29/04/2015.
 */

@Constraint(validatedBy = {substring_validator.class})
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
public @interface substring_constraint {

    String message() default "This string must contain the xg_attribute_1 attribute ";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    String xg_attribute_1() default "";
}
