package test2_constraints_validation;

import javax.validation.constraints.*;

/**
 * Created by Giovanni on 29/04/2015.
 */
public class class_with_builtin_constraints {



        @NotNull @Size(min = 4, max = 50)
        private String title;
        @NotNull
        private Float price;


        @Pattern(regexp = "[A-Z][a-z]{1,}")
        private String musicCompany;

        @Max(value = 5)
        private Integer numberOfCDs;

        @NotNull @DecimalMin("5.8")
        public Float calculatePrice(@DecimalMin("1.4") Float rate) {
            return price * rate;
        }
        @DecimalMin("9.99")
        public Float calculateVAT() {
            return price * 0.196f;
        }

        @AssertTrue
        public Boolean CheckStringLength (@NotNull String a) {
            return a.length() > 0;
        }


}
