package test2_constraints_validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Giovanni on 29/04/2015.
 */
public class xg_substring_validator implements ConstraintValidator<xg_substring_constraint,String> {

    @Override
    public void initialize(xg_substring_constraint xg_substring_constraint) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s.contains("xg");
    }
}
