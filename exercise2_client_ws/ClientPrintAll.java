package exercise2_client_ws;

import exercise2_ws.Book;
import exercise2_ws.RemoteInterface;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Iterator;
import java.util.List;

public class ClientPrintAll {


        public static void main (String[] args) {


            Context ctx = null;
            try {
                ctx =new InitialContext();
            } catch (NamingException e) {
                e.printStackTrace();
            }

            RemoteInterface r = null;
            try {
                r =(RemoteInterface) ctx.lookup("java:global/web/EJBBook!exercise2_ws.RemoteInterface");
                List<Book> l =  r.findall();
                Iterator<Book> it = l.iterator();
                while (it.hasNext()){
                    Book b = it.next();
                    System.out.print(b.getIsbn() + " " + b.getTitle() + " " + b.getAuthor() + " " +b.getPrice() + " ");
                }

            } catch (NamingException e) {
                e.printStackTrace();
            }



        }


}
