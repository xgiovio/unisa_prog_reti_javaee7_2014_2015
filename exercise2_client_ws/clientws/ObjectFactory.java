
package exercise2_client_ws.clientws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the exercise2_client_ws.clientws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Checkbook_QNAME = new QName("http://exercise2_ws/", "checkbook");
    private final static QName _CheckbookResponse_QNAME = new QName("http://exercise2_ws/", "checkbookResponse");
    private final static QName _Book_QNAME = new QName("http://exercise2_ws/", "book");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: exercise2_client_ws.clientws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Book }
     * 
     */
    public Book createBook() {
        return new Book();
    }

    /**
     * Create an instance of {@link Checkbook }
     * 
     */
    public Checkbook createCheckbook() {
        return new Checkbook();
    }

    /**
     * Create an instance of {@link CheckbookResponse }
     * 
     */
    public CheckbookResponse createCheckbookResponse() {
        return new CheckbookResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Checkbook }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://exercise2_ws/", name = "checkbook")
    public JAXBElement<Checkbook> createCheckbook(Checkbook value) {
        return new JAXBElement<Checkbook>(_Checkbook_QNAME, Checkbook.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckbookResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://exercise2_ws/", name = "checkbookResponse")
    public JAXBElement<CheckbookResponse> createCheckbookResponse(CheckbookResponse value) {
        return new JAXBElement<CheckbookResponse>(_CheckbookResponse_QNAME, CheckbookResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Book }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://exercise2_ws/", name = "book")
    public JAXBElement<Book> createBook(Book value) {
        return new JAXBElement<Book>(_Book_QNAME, Book.class, null, value);
    }

}
