
package exercise2_client_ws.clientws;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.10-b140803.1500
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "BookWebServiceService", targetNamespace = "http://exercise2_ws/", wsdlLocation = "http://localhost:8080/web/services/BookWebService?wsdl")
public class BookWebServiceService
    extends Service
{

    private final static URL BOOKWEBSERVICESERVICE_WSDL_LOCATION;
    private final static WebServiceException BOOKWEBSERVICESERVICE_EXCEPTION;
    private final static QName BOOKWEBSERVICESERVICE_QNAME = new QName("http://exercise2_ws/", "BookWebServiceService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/web/services/BookWebService?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        BOOKWEBSERVICESERVICE_WSDL_LOCATION = url;
        BOOKWEBSERVICESERVICE_EXCEPTION = e;
    }

    public BookWebServiceService() {
        super(__getWsdlLocation(), BOOKWEBSERVICESERVICE_QNAME);
    }

    public BookWebServiceService(WebServiceFeature... features) {
        super(__getWsdlLocation(), BOOKWEBSERVICESERVICE_QNAME, features);
    }

    public BookWebServiceService(URL wsdlLocation) {
        super(wsdlLocation, BOOKWEBSERVICESERVICE_QNAME);
    }

    public BookWebServiceService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, BOOKWEBSERVICESERVICE_QNAME, features);
    }

    public BookWebServiceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public BookWebServiceService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns Wsinterface
     */
    @WebEndpoint(name = "BookWebServicePort")
    public Wsinterface getBookWebServicePort() {
        return super.getPort(new QName("http://exercise2_ws/", "BookWebServicePort"), Wsinterface.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns Wsinterface
     */
    @WebEndpoint(name = "BookWebServicePort")
    public Wsinterface getBookWebServicePort(WebServiceFeature... features) {
        return super.getPort(new QName("http://exercise2_ws/", "BookWebServicePort"), Wsinterface.class, features);
    }

    private static URL __getWsdlLocation() {
        if (BOOKWEBSERVICESERVICE_EXCEPTION!= null) {
            throw BOOKWEBSERVICESERVICE_EXCEPTION;
        }
        return BOOKWEBSERVICESERVICE_WSDL_LOCATION;
    }

}
