package exercise2_client_ws.clientws;


/**
 * Created by Giovanni on 13/06/2015.
 */
public class ClientSendBook {

    public static void main (String[] args) {

        Wsinterface bws = new BookWebServiceService().getBookWebServicePort();

        Book b = new Book();
        b.setTitle("xgiovio president");
        b.setPrice(100f);
        b.setAuthor("xgiovio");
        b.setIsbn("0001s");

        System.out.println(bws.checkbook( b));



    }



}
