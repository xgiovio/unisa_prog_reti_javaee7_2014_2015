package exercise2_client_ws;

import exercise2_ws.BookMessage;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Created by Giovanni on 31/05/2015.
 */
public class ClientUpdatePrice {

    public static void main(String[] args) throws NamingException {


        BookMessage m = new BookMessage("0001",Float.valueOf(10000));

        Context jndiContext = new InitialContext();

        ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext.lookup("jms/javaee7/ConnectionFactory");
        Destination topic = (Destination) jndiContext.lookup("jms/javaee7/Topic");
        try (JMSContext jmsContext = connectionFactory.createContext()) {
            jmsContext.createProducer().send(topic, m);
        }
    }







}
