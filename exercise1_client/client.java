package exercise1_client;

import exercise1.RemoteInterface;
import exercise1.Student;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Giovanni on 24/05/2015.
 */
public class client {

    public static void main (String[] args) {


        Context ctx = null;
        try {
            ctx =new InitialContext();
        } catch (NamingException e) {
            e.printStackTrace();
        }

        RemoteInterface r = null;
        try {
            r =(RemoteInterface) ctx.lookup("java:global/web/EJB!exercise1.RemoteInterface");
            List<Student>l =  r.findname("mario");
            Iterator<Student> it = l.iterator();
            System.out.println(it.next().getSurname());
            System.out.println(it.next().getSurname());


        } catch (NamingException e) {
            e.printStackTrace();
        }



    }
}