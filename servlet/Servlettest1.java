package servlet;

import test1_cdi.*;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Giovanni on 25/04/2015.
 */
@WebServlet(name = "Servlettest1")
public class Servlettest1 extends HttpServlet {

    @Inject @ClassI
    test1_cdi.Class c;

    protected void processRequest(HttpServletRequest request,  HttpServletResponse response)   throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
/* TODO output your page here. */

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet xg</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet xg at " + request.getContextPath() + "</h1>");

            out.println(c.printStudent());



            out.println("</body>");
            out.println("</html>");
        }
    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

    }
}
