package servlet;

import test3_persistence.store_class;
import test3_persistence.test_class;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Giovanni on 25/04/2015.
 */

@WebServlet(name = "Servletexercise1")
public class Servletexercise1 extends HttpServlet {


    protected void processRequest(HttpServletRequest request,  HttpServletResponse response)   throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
/* TODO output your page here. */

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet xg</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet xg at " + request.getContextPath() + "</h1>");



            out.println("</body>");
            out.println("</html>");
        }
    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

    }
}
