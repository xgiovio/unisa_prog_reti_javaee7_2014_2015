package servlet;

import test2_constraints_validation.*;


import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.executable.ExecutableValidator;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Giovanni on 25/04/2015.
 */
@WebServlet(name = "Servlettest2")
public class Servlettest2 extends HttpServlet {

    @Inject
    ValidatorFactory validatorFactory;
    @Inject
    Validator validator;


    @Inject
    xg_substring_class xg_substring_class;

    @Inject
    substring_class substring_class;

    @Inject
    general_class_wrapper general_class_wrapper;

    @Inject
    general_class general_class;

    @Inject
    class_with_builtin_constraints class_with_builtin_constraints;


    @Inject
    class_test_method class_test_method;


    protected void processRequest(HttpServletRequest request,  HttpServletResponse response)   throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
/* TODO output your page here. */

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet xg</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet xg at " + request.getContextPath() + "</h1>");


            ///////////////// check null return
            //class_test_method.test_method();
            //out.println("Test Null without validator");
            //out.println("<br>");


            ///////////////// entire class validation

            Set<ConstraintViolation<xg_substring_class>> violations = validator.validate(xg_substring_class);
            if (violations.size() > 0) out.println("error"); else out.println("tutto ok");

            out.println("<br>");

            Set<ConstraintViolation<substring_class>> violations2 = validator.validate(substring_class);
            if (violations2.size() > 0) out.println("error"); else out.println("tutto ok");

            out.println("<br>");

            Set<ConstraintViolation<general_class_wrapper>> violations3 = validator.validate(general_class_wrapper);
            if (violations3.size() > 0) {
                out.println("error");
                Iterator<ConstraintViolation<general_class_wrapper>> it = violations3.iterator();
                while (it.hasNext()){
                    ConstraintViolation<general_class_wrapper> t = it.next();
                    out.println(t.getInvalidValue() + " " + t.getMessage());
                }

            } else{
                out.println("tutto ok");
            }


            out.println("<br>");

            Set<ConstraintViolation<general_class>> violations4 = validator.validate(general_class);
            if (violations4.size() > 0) out.println("error"); else out.println("tutto ok");

            out.println("<br>");

            //////////////// single method paramenter validation

            Method method = null;
            try {
                method = class_with_builtin_constraints.class.getMethod("calculatePrice", Float.class);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            ExecutableValidator methodValidator = validator. forExecutables();
            Set<ConstraintViolation<class_with_builtin_constraints>> violations5 = methodValidator.validateParameters(class_with_builtin_constraints, method, new Object[]{new Float(2)});
            if (violations5.size() > 0) out.println("error"); else out.println("tutto ok");

            out.println("<br>");
            //////////////// single method return value parameter validation

            method = null;
            try {
                method = class_with_builtin_constraints.class.getMethod("calculatePrice", Float.class);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
             methodValidator = validator. forExecutables();
            Set<ConstraintViolation<class_with_builtin_constraints>> violations6 = methodValidator.validateReturnValue(class_with_builtin_constraints, method, new Float(6));
            if (violations6.size() > 0) out.println("error"); else out.println("tutto ok");


            out.println("<br>");
            //////////////// check single property without make an instance of the object
            Set<ConstraintViolation<class_with_builtin_constraints>> violations7 = validator.validateValue(class_with_builtin_constraints.class, "title", "aabb");
            if (violations7.size() > 0) out.println("error"); else out.println("tutto ok");












            out.println("</body>");
            out.println("</html>");
        }
    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

    }
}
