package servlet;

import test2_constraints_validation.*;
import test3_persistence.store_class;
import test3_persistence.test_class;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.*;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.executable.ExecutableValidator;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by Giovanni on 25/04/2015.
 */

@WebServlet(name = "Servlettest3")
public class Servlettest3 extends HttpServlet {


    @EJB store_class s;

    protected void processRequest(HttpServletRequest request,  HttpServletResponse response)   throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
/* TODO output your page here. */

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet xg</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet xg at " + request.getContextPath() + "</h1>");


            s.store_class(new test_class());
            out.println(s.find(1L).getTitle());
            //s.fake_remove(1L);
            s.remove_and_persist(1L);

            test_class t = s.find(1L);
            t.setTitle("alibaa");
            out.println(s.find(1L).getTitle()); // database isn't updated
            out.println(s.contains(s.find(1L))); // it's false
            s.merge(t);
            out.println(s.find(1L).getTitle());// database is updated! All OK!

            //test_class q = (test_class) s.namedquery_single("findtest_class") ;
            //out.println(q.getTitle());

            /*
            List<Object> q = s.namedquery_multiple("findtest_class") ;
            Iterator<Object> it = q.iterator();
            while (it.hasNext()){
                out.println(((test_class) it.next()).getTitle());
            }

            */
            out.println("<br>");

            List<Object> q2 = s.dynquery_multiple("SELECT t FROM test_class t") ;
            Iterator<Object> it = q2.iterator();
            while (it.hasNext()){
                out.println(((test_class) it.next()).getTitle());
            }

            out.println("<br>");

            HashMap<String,String> h = new HashMap<String,String>();
            h.put("fname", "alibaa");
            // h.put(...);
            List<Object> q3 = s.dynquery_multiple_string("SELECT t FROM test_class t where t.title = :fname", h) ;
            Iterator<Object> it2 = q3.iterator();
            while (it2.hasNext()){
                out.println(((test_class) it2.next()).getTitle());
            }

            out.println("<br>");

            HashMap<Integer,String> h2 = new HashMap<Integer,String>();
            h2.put(1,"alibaa");
            // h2.put(...);
            List<Object> q4 = s.dynquery_multiple_integer("SELECT t FROM test_class t where t.title = ?1", h2) ;
            Iterator<Object> it3 = q4.iterator();
            while (it3.hasNext()){
                out.println(((test_class) it3.next()).getTitle());
            }



















            out.println("</body>");
            out.println("</html>");
        }
    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

    }
}
