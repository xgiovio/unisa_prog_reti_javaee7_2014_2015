package test5_ws;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Giovanni on 12/06/2015.
 */
@XmlRootElement
public class ObjectToPass {
    public String message = "def";

}
