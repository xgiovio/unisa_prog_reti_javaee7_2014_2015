package test5_ws;

import javax.jws.WebService;

/**
 * Created by Giovanni on 12/06/2015.
 */
@WebService
public interface HelloWorldInterface {
    ObjectToPass modifymessage(ObjectToPass o);
}
