package test5_ws;
import javax.ejb.Stateless;
import javax.jws.WebService;


/**
 * Created by Giovanni on 12/06/2015.
 */
@WebService(endpointInterface ="test5_ws.HelloWorldInterface")
@Stateless
public class HelloWorld implements  HelloWorldInterface{
  public ObjectToPass modifymessage(ObjectToPass o) {
    System.out.println("original message " + o.message);
    o.message = "modified by server";
    return o;
  }
}
