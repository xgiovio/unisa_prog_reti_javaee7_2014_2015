
package test5_ws_client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the test5_ws_client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Modifymessage_QNAME = new QName("http://test5_ws/", "modifymessage");
    private final static QName _ObjectToPass_QNAME = new QName("http://test5_ws/", "objectToPass");
    private final static QName _ModifymessageResponse_QNAME = new QName("http://test5_ws/", "modifymessageResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: test5_ws_client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ModifymessageResponse }
     * 
     */
    public ModifymessageResponse createModifymessageResponse() {
        return new ModifymessageResponse();
    }

    /**
     * Create an instance of {@link ObjectToPass }
     * 
     */
    public ObjectToPass createObjectToPass() {
        return new ObjectToPass();
    }

    /**
     * Create an instance of {@link Modifymessage }
     * 
     */
    public Modifymessage createModifymessage() {
        return new Modifymessage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Modifymessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://test5_ws/", name = "modifymessage")
    public JAXBElement<Modifymessage> createModifymessage(Modifymessage value) {
        return new JAXBElement<Modifymessage>(_Modifymessage_QNAME, Modifymessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectToPass }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://test5_ws/", name = "objectToPass")
    public JAXBElement<ObjectToPass> createObjectToPass(ObjectToPass value) {
        return new JAXBElement<ObjectToPass>(_ObjectToPass_QNAME, ObjectToPass.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifymessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://test5_ws/", name = "modifymessageResponse")
    public JAXBElement<ModifymessageResponse> createModifymessageResponse(ModifymessageResponse value) {
        return new JAXBElement<ModifymessageResponse>(_ModifymessageResponse_QNAME, ModifymessageResponse.class, null, value);
    }

}
