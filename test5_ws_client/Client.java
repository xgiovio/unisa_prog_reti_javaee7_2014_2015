package test5_ws_client;

import test5_ws.*;

/**
 * Created by Giovanni on 12/06/2015.
 */
public class Client {
    public static void main(String[] argv) {
        HelloWorldInterface service = new HelloWorldService().getHelloWorldPort();
        //invoke business method
        ObjectToPass o = new ObjectToPass();
        o.message = "jo from the client";
        System.out.println(service.modifymessage(o).message);
    }
}

