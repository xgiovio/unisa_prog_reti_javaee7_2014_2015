package test1_cdi;

/**
 * Created by Giovanni on 26/04/2015.
 */
import javax.enterprise.inject.Alternative;

@Alternative @StudentI(implementation = student_implementations.one)
public class StudentAlternative implements Student {

    @Override
    public String getinfo() {
        return "i'm an alternative";
    }
}
