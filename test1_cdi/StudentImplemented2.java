package test1_cdi;

/**
 * Created by Giovanni on 26/04/2015.
 */

@StudentI(implementation = student_implementations.two)
public class StudentImplemented2 implements Student {

    @Override
    public String getinfo() {
        return "null null 2";
    }
}
