package test1_cdi;


import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

/**
 * Created by Giovanni on 27/04/2015.
 */

@Decorator
public class ClassDecorator implements ClassInterface {

    @Inject @Delegate @ClassI
    private ClassInterface c;


    public String printStudent() {
        return c.printStudent() + " decorated ";
    }



}
