package test1_cdi;

import javax.enterprise.inject.*;
import javax.inject.Inject;
import java.util.logging.Logger;

/**
 * Created by Giovanni on 26/04/2015.
 */


public class ClassProducer {

    @Inject
    Logger l;

    @Inject @StudentI(implementation = student_implementations.one)
    Student s;

    @Produces @NumberOfStudents
    private int number_of_students(){

        return 10;

    }

    @Produces
    private int number_of_students_2(){

        return 20;

    }


    private void clean (@Disposes  int s) {
        l.info("Do something to clean the producer");

    }

    private void clean2(@Disposes @NumberOfStudents int s) {
        l.info("Do something to clean the producer");

    }



}
