package test1_cdi;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import javax.interceptor.InterceptorBinding;

/**
 * Created by Giovanni on 27/04/2015.
 */

@InterceptorBinding
@Target({METHOD, TYPE, CONSTRUCTOR})
@Retention(RUNTIME)
public @interface GeneralInterceptorA {
}
