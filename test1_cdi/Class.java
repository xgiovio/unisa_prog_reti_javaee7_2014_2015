package test1_cdi;


import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.logging.Logger;
import javax.enterprise.event.Event;

/**
 * Created by Giovanni on 26/04/2015.
 */

@ClassI
@GeneralInterceptorA
public class Class implements ClassInterface {

    @Inject
    Logger l;

    @Inject @StudentI(implementation = student_implementations.two)
    Student s;

    @Inject @NumberOfStudents
    int number_of_students;

    @Inject @Event1
    Event<Object> event_shooter1;

    @Inject @Event2
    Event<Object> event_shooter2;

    @PostConstruct
    public void do_post_construct(){
        l.info("XGLOG: Post Construct done");
    }


    public String printStudent (){
        event_shooter1.fire(new Object());
        event_shooter2.fire(new Object());
        return (s.getinfo() +" " + number_of_students);
    }


    public void observe1 (@Observes @Event1 Object b){
        l.info("XGLOG: Event1 captured!");
    }

    public void observe2 (@Observes @Event2 Object b){
        l.info("XGLOG: Event2 captured!");
    }

}
