package test1_cdi;

/**
 * Created by Giovanni on 27/04/2015.
 */

import javax.annotation.PostConstruct;
import javax.annotation.Priority;
import javax.inject.Inject;
import javax.interceptor.*;
import java.util.logging.Logger;

@Interceptor
@GeneralInterceptorA
@Priority(200)

public class GeneralInterceptor {

    @Inject
    private Logger logger;


    @AroundConstruct
    private void logConstruct(InvocationContext ic) throws Exception {
        logger.info("XGLOG: Begin Constructor Interceptor");
        try {
            ic.proceed();
        } finally {
            logger.info("XGLOG: End Constructor Interceptor");
        }
    }


    @PostConstruct
    public void logPostConstruct (InvocationContext ic) throws Exception {
        logger.info("XGLOG: Begin PostConstructor Interceptor");
        try {
            ic.proceed();
        } finally {
            logger.info("XGLOG: End PostConstructor Interceptor");
        }
    }


    @AroundInvoke
    public Object logMethod(InvocationContext ic) throws Exception {
        logger.info("XGLOG: Begin " + ic.getTarget().toString() + " "+ ic.getMethod().getName()+ " Interceptor");
        try {
            return ic.proceed();
        } finally {
            logger.info("XGLOG: End " +  ic.getTarget().toString() + " "+ ic.getMethod().getName() + " Interceptor");
        }
    }




}
