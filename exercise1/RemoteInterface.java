package exercise1;

import javax.ejb.Remote;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Giovanni on 24/05/2015.
 */
@Remote
public interface RemoteInterface {

    @NotNull List<Student> findname (@NotNull String name);
    @NotNull List<Student> findsurname (@NotNull String name) ;
    @NotNull List<Student> findyear (@NotNull String name) ;

}
