package exercise1;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import javax.inject.Inject;

/**
 * Created by Giovanni on 24/05/2015.
 */

//@Singleton   enable
//@Startup      enable
public class InitializeDB {

    private Student s1,s2;

    @Inject
    private EJB e;

    @PostConstruct
    void pupulate (){
            s1 = new Student("mario","marra","1");
            s2 = new Student("mario","maria","2");
            e.create_student(s1);
            e.create_student(s2);
    }

    @PreDestroy
    void delete () {
        e.delete_student(s1);
        e.delete_student(s2);
    }






}
