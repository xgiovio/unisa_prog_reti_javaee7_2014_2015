package exercise1;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Giovanni on 24/05/2015.
 */
@Stateless
@LocalBean
public class EJB implements RemoteInterface {

    @PersistenceContext(unitName = "exercise1")
    EntityManager em ;


    public void create_student (@NotNull Student s) {
        em.persist(s);
    }
    public void update_student (@NotNull Student s) {
        em.merge(s);
        em.persist(s);
    }
    public void delete_student (@NotNull Student s) {
        em.merge(s);
        em.remove(s);
    }

    public @NotNull List<Student> findname (@NotNull String name) {
        Query Q = em.createNamedQuery("findname");
        Q.setParameter(1, name);
        return (List<Student>)Q.getResultList();
    }

    public @NotNull List<Student> findsurname (@NotNull String name) {
        Query Q = em.createNamedQuery("findsurname");
        Q.setParameter(1,name);
        return (List<Student>)Q.getResultList();
    }

    public @NotNull List<Student> findyear (@NotNull String name) {
        Query Q = em.createNamedQuery("findyear");
        Q.setParameter(1,name);
        return (List<Student>)Q.getResultList();
    }


}
