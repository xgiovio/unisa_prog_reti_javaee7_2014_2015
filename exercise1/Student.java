package exercise1;

import javax.ejb.Local;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Giovanni on 24/05/2015.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "findname", query = "SELECT s FROM Student s WHERE s.name =?1"),
        @NamedQuery(name = "findsurname", query = "SELECT s FROM Student s WHERE s.surname =?1"),
        @NamedQuery(name = "findyear", query = "SELECT s FROM Student s WHERE s.years =?1")
})
public class Student implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String name;
    @NotNull
    private String surname;
    @NotNull
    private String years;


    public Student(){}

    public Student (@NotNull String in_name, @NotNull String in_surname, @NotNull String in_year){

        name = in_name;
        surname = in_surname;
        years = in_year;

    }

    public @NotNull String getName() {
        return name;
    }

    public @NotNull String getSurname() {
        return surname;
    }

    public @NotNull String getYears() {
        return years;
    }
}
