package exercise2;



import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Giovanni on 24/05/2015.
 */
@Stateless
@LocalBean
public class EJBBook implements RemoteInterface {

    @PersistenceContext(unitName = "exercise2")
    EntityManager em ;


    public void create_book (@NotNull Book s) {
        em.persist(s);
    }
    public void update_book (@NotNull Book s) {
        em.merge(s);
        em.persist(s);
    }
    public void delete_book (@NotNull Book s) {
        em.merge(s);
        em.remove(s);
    }

    public @NotNull List<Book> findbyisbn (@NotNull String in_isbn) {
        Query Q = em.createNamedQuery("findbyisbn");
        Q.setParameter(1, in_isbn);
        return (List<Book>)Q.getResultList();
    }


    public  List<Book> findall () {
        Query Q = em.createNamedQuery("findall");
        return (List<Book>)Q.getResultList();
    }


}
