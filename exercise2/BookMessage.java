package exercise2;

import java.io.Serializable;

/**
 * Created by Giovanni on 31/05/2015.
 */
public class BookMessage implements Serializable{

    public BookMessage (String in_isbn, Float in_price){
        isbn = in_isbn;
        price = in_price;
    }


    private String isbn;
    private Float price;

    public String getIsbn() {
        return isbn;
    }

    public Float getPrice() {
        return price;
    }
}
