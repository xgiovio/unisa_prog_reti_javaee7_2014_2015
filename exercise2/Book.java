package exercise2;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Giovanni on 31/05/2015.
 */

@Entity
@NamedQueries({
        @NamedQuery(name = "findbyisbn", query = "SELECT b FROM Book b WHERE b.isbn =?1"),
        @NamedQuery(name = "findall", query = "SELECT b FROM Book b")
})
public class Book implements Serializable{

    @GeneratedValue @Id
    private Long id ;
    @NotNull
    private String title;
    @NotNull
    private String author;
    @NotNull
    private Float price;
    @NotNull
    private String isbn;


    public Book(){}

    public Book(@NotNull String in_isbn,@NotNull String in_title,@NotNull String in_author, @NotNull @DecimalMin("0.0") Float in_price){
        title = in_title;
        author = in_author;
        price = in_price;
        isbn = in_isbn;
    }

    public @NotNull String getIsbn() {
        return isbn;
    }

    public void setIsbn(@NotNull String isbn) {
        this.isbn = isbn;
    }

    public @NotNull String getTitle() {
        return title;
    }

    public void setTitle(@NotNull String title) {
        this.title = title;
    }

    public @NotNull String getAuthor() {
        return author;
    }

    public void setAuthor( @NotNull String author) {
        this.author = author;
    }

    public @NotNull @DecimalMin("0.0") Float getPrice() {
        return price;
    }

    public void setPrice(@NotNull @DecimalMin("0.0") Float price) {
        this.price = price;
    }
}
