package exercise2;



import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 * Created by Giovanni on 24/05/2015.
 */

//@Singleton     reenable if you need this exercise
//@Startup
public class InitializeDB {

    private Book b1,b2;

    @Inject
    private EJBBook e;

    @PostConstruct
    void pupulate (){
            b1 = new Book("0001","Titolo1","Author1",10f);
            b2 = new Book("0010","Titolo2","Author2",30f);
            e.create_book(b1);
            e.create_book(b2);
    }

    @PreDestroy
    void delete () {
        e.delete_book(b1);
        e.delete_book(b2);
    }






}
