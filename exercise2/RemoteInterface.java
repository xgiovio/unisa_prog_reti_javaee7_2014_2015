package exercise2;

import exercise1.Student;

import javax.ejb.Remote;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Giovanni on 24/05/2015.
 */
@Remote
public interface RemoteInterface {

    @NotNull List<Book> findbyisbn(@NotNull String in_isbn);
     List<Book> findall() ;

}
