package exam;



import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Giovanni on 16/06/2015.
 */

@MessageDriven(mappedName = "jms/javaee7/Topic")
public class MDB implements MessageListener {

    @Inject
    EJB e;


    public void onMessage(Message m) {

        try {
            GeneralMessage gm = m.getBody(GeneralMessage.class);
            List<CD> l = e.findid(gm.getId());
            Iterator<CD> i = l.iterator();
            if (i.hasNext()){
                CD temp = i.next();
                temp.setPrice(gm.getPrice());
                e.updateCD(temp);
            }

        } catch (JMSException e) { // added try catch for JMSException
            e.printStackTrace();
        }


    }

}
