package exam;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Iterator;
import java.util.List;

public class ClientPrintAllCDs {


        public static void main (String[] args) throws NamingException{
            // added method main and signature exception

            Context ctx =new InitialContext();

            RemoteEJB r = (RemoteEJB) ctx.lookup("java:global/web/EJB!exam.RemoteEJB");
                List<CD> l =  r.findall();
                Iterator<CD> i = l.iterator();
                while (i.hasNext()){
                    CD temp = i.next();
                    System.out.print(temp.getId() + " " + temp.getTitle() + " " + temp.getAuthor() + " " +temp.getPrice() + "\n");
                }
        }


}
