package exam;

import java.io.Serializable;

/**
 * Created by Giovanni on 16/06/2015.
 */
public class GeneralMessage implements Serializable{

    private String id;
    private Float price;

    public GeneralMessage (String in_id, Float in_price){
        id = in_id;
        price = in_price;
    }

    public String getId() {
        return id;
    }

    public Float getPrice() {
        return price;
    }
}
