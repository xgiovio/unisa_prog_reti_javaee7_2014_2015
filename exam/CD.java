package exam;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Giovanni on 16/06/2015.
 */

@Entity
@NamedQueries({
        @NamedQuery(name = "findid", query = "SELECT c FROM CD c WHERE c.id =?1"),
        @NamedQuery(name = "findauthor", query = "SELECT c FROM CD c WHERE c.author =?1"),
        @NamedQuery(name = "findall", query = "SELECT c FROM CD c"),
})

public class CD implements Serializable {

    @NotNull @Id
    private  String id;
    @NotNull
    private String title;
    @NotNull
    private String author;
    @NotNull @DecimalMin("0.0")
    private Float price;

    public CD (){}

    public CD (@NotNull String id_in,@NotNull String title_in,@NotNull String author_in,@NotNull @DecimalMin("0.0") Float price_in){

        id = id_in;
        title = title_in;
        author = author_in;
        price = price_in;

    }


    public @NotNull String getId() {
        return id;
    }


    public @NotNull String getTitle() {
        return title;
    }

    public void setTitle(@NotNull String title) {
        this.title = title;
    }

    public @NotNull @DecimalMin("0.0") Float getPrice() {
        return price;
    }

    public void setPrice(@NotNull @DecimalMin("0.0") Float price) {
        this.price = price;
    }

    public @NotNull String getAuthor() {
        return author;
    }

    public void setAuthor(@NotNull String author) {
        this.author = author;
    }
}
