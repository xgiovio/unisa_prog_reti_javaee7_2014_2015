package exam;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 * Created by Giovanni on 16/06/2015.
 */

@Singleton
@Startup

public class DatabaseInitializer {

    @Inject
    EJB e;

    private CD first;
    private CD second;

    @PostConstruct
    protected void initialize(){
        first = new CD("001","Title1","Author1",35.0f);
        e.createCD(first);
        second = new CD("002","Title2","Author2",40.0f);
        e.createCD(second);
    }

    @PreDestroy
    protected void deleteall(){

        e.removeCD(first);
        e.removeCD(second);

    }

}
