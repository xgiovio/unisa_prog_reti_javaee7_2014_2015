package exam;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Giovanni on 16/06/2015.
 */

@Stateless
@LocalBean

public class EJB implements RemoteEJB{

    @PersistenceContext(unitName = "exam")
    EntityManager em ;


    public void createCD (CD c){
        em.persist(c);
    }

    public void updateCD (CD c) {
        em.merge(c);
        em.persist(c);
    }

    public void removeCD (CD c){
        em.merge(c);
        em.remove(c);
    }


    public List<CD> findall() {
        Query q = em.createNamedQuery("findall");
        return (List<CD>) q. getResultList();

    }


    public List<CD> findauthor(String author_in) {
        Query q = em.createNamedQuery("findauthor");
        q.setParameter(1,author_in);
        return (List<CD>) q. getResultList();
    }


    public List<CD> findid(String id_in) {
        Query q = em.createNamedQuery("findid");
        q.setParameter(1,id_in);
        return (List<CD>) q. getResultList();
    }
}
