package exam;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Created by Giovanni on 31/05/2015.
 */
public class ClientUpdatePrice {

    public static void main(String[] args) throws NamingException {
        //removed try catch e added signatute exception to main method

        Context ctx = new InitialContext();

        ConnectionFactory cf = (ConnectionFactory) ctx.lookup("jms/javaee7/ConnectionFactory");
        Destination d = (Destination) ctx.lookup("jms/javaee7/Topic");
        try (JMSContext jmsContext = cf.createContext()) {
            GeneralMessage gm = new GeneralMessage(args[0],Float.valueOf(args[1]));
            jmsContext.createProducer().send(d, gm);
        }
    }







}
