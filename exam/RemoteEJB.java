package exam;

import javax.ejb.Remote;
import java.util.List;

/**
 * Created by Giovanni on 16/06/2015.
 */
@Remote
public interface RemoteEJB {

    List<CD> findall();
    List<CD> findauthor(String author_in);
    List<CD> findid(String id_in);

}
