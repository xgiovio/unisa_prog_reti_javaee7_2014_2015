package test3_client;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;
import test3_persistence.store_class_remote_interface;

/**
 * Created by Giovanni on 17/05/2015.
 */
public class client {

    public static void main (String[] args) {


        Context ctx = null;
        try {
            ctx =new InitialContext();
        } catch (NamingException e) {
            e.printStackTrace();
        }

        Object foo = null;
        try {
            foo = ctx.lookup("java:global/web/store_class!test3_persistence.store_class_remote_interface");

        } catch (NamingException e) {
            e.printStackTrace();
        }



    }

}
