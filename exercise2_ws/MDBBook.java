package exercise2_ws;

import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Giovanni on 31/05/2015.
 */
@MessageDriven(mappedName = "jms/javaee7/Topic")
public class MDBBook implements MessageListener {

    @Inject
    Logger l;

    @Inject @ws
    EJBBook e;

    public void onMessage(Message message) {
        try {
            exercise2_ws.BookMessage m = message.getBody(exercise2_ws.BookMessage.class);
            l.info("Messaggio: " + m.getIsbn() + " " + m.getPrice());

            List<exercise2_ws.Book> lb = e.findbyisbn(m.getIsbn());
            Iterator<exercise2_ws.Book>ib = lb.iterator();
            if (ib.hasNext()){
                Book b = ib.next();
                b.setPrice(m.getPrice());
                e.update_book(b);
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
