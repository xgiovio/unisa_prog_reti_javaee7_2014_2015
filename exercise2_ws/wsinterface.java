package exercise2_ws;

import javax.jws.WebService;

/**
 * Created by Giovanni on 13/06/2015.
 */
@WebService
public interface wsinterface {
    boolean checkbook(Book b);
}
