package exercise2_ws;

import javax.ejb.Remote;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Giovanni on 24/05/2015.
 */
@Remote
public interface RemoteInterface {

    @NotNull List<exercise2_ws.Book> findbyisbn(@NotNull String in_isbn);
     List<exercise2_ws.Book> findall() ;

}
