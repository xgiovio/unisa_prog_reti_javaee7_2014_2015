package test3_persistence;

import javax.ejb.Remote;

/**
 * Created by Giovanni on 17/05/2015.
 */
@Remote
public interface store_class_remote_interface {

    test_class find (Long n);

}
