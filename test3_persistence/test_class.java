package test3_persistence;


import test1_cdi.GeneralInterceptorA;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "findtest_class", query = "SELECT b FROM test_class b WHERE b.title ='test_class'"),
        @NamedQuery(name = "findxgio", query = "SELECT b FROM test_class b WHERE b.title ='xgio'"),
        @NamedQuery(name = "findxg", query = "SELECT b FROM test_class b WHERE b.title ='xg'")
})
@EntityListeners({Persistent_Listener1.class,Persistent_Listener2.class})

public class test_class {

        @Id @GeneratedValue
        private Long id;


        public test_class (){
                title = "test_class";
                xlist = new ArrayList<test_class_2>();
                xlist.add(new test_class_2());
                xlist.get(0).setT(this);
                xlist.add(new test_class_2());
                xlist.get(1).setT(this);

        }

        private String title;

        //@NotNull
        private Double price;

        @OneToMany (mappedBy = "t",fetch = FetchType.LAZY, cascade = {CascadeType.ALL},orphanRemoval=true)
        private List<test_class_2> xlist;

        public String getTitle() {
                return title;
        }

        public void setTitle(String title) {
                this.title = title;
        }

        public Double getPrice() {
                return price;
        }

        public void setPrice(Double price) {
                this.price = price;
        }

        public List<test_class_2> getXlist() {
                return xlist;
        }

        public void setXlist(List<test_class_2> xlist) {
                this.xlist = xlist;
        }
}
