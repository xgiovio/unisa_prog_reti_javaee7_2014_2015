package test3_persistence;


import test1_cdi.GeneralInterceptorA;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@GeneralInterceptorA
@NamedQuery(name = "findxg2", query = "SELECT b FROM test_class b WHERE b.title ='xg2'")
public class test_class_2 {

        @Id @GeneratedValue
        private Long id;


        public test_class_2 (){
                title = "test_class_2";
        }

        private String title;

        //@NotNull
        private Double price;

        @ManyToOne (fetch = FetchType.LAZY)
        private test_class t;

        public test_class getT() {
                return t;
        }

        public void setT(test_class t) {
                this.t = t;
        }

        public String getTitle() {
                return title;
        }

        public void setTitle(String title) {
                this.title = title;
        }

        public Double getPrice() {
                return price;
        }

        public void setPrice(Double price) {
                this.price = price;
        }
}
