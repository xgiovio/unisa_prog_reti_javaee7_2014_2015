package test3_persistence;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Created by Giovanni on 04/05/2015.
 */

@Stateless
@LocalBean
public class store_class implements Serializable,store_class_remote_interface {

    @Inject
    Logger l;

    @PersistenceContext(unitName = "test3")
    private EntityManager em;


    public void store_class(test_class t){

        em.persist(t);

    }

    public test_class find (Long n){
        //return em.find(test_class.class,n); // null if fail
        //or
        return em.getReference(test_class.class, n); // EntityNotFoundException if fail
    }


    public void fake_remove (Long n){

        test_class t = find(n);
        t.setXlist(null); // works because orphanremoval is true

    }

    public void remove_and_persist (Long n){
        test_class t = find(n);
        em.remove(t); // delete t object and connected objects because cascading is set to CascadeType.ALL
        em.persist(t); // persist work, objects are in memory, all is safe again
    }

    public boolean contains (test_class t){
        return em.contains(t);
    }

    public void merge (test_class t){
        //em.persist(t); error, object already present in database
        em.merge(t);
    }


    public Object  namedquery_single (String q){
        Query Q = em.createNamedQuery(q);
        return Q.getSingleResult(); // if no result NoResultException
                                    // if more NonUniqueResultException
    }

    public List<Object> namedquery_multiple (String q){
        Query Q = em.createNamedQuery(q);
        return Q.getResultList(); // if no result NoResultException

    }

    public List<Object> dynquery_multiple (String q){
        Query Q = em.createQuery(q);
        return Q.getResultList(); // if no result NoResultException

    }

    public List<Object> dynquery_multiple_string (String q, HashMap<String,String> h){
        Query Q = em.createQuery(q);
        Iterator<String> it =  h.keySet().iterator();
        while (it.hasNext()){
            String next = it.next();
            l.info(next);
            l.info(h.get(next));
            Q.setParameter(next,h.get(next));
        }
        return Q.getResultList(); // if no result NoResultException

    }


    public List<Object> dynquery_multiple_integer (String q, HashMap<Integer,String> h){
        Query Q = em.createQuery(q);
        Iterator<Integer> it =  h.keySet().iterator();
        while (it.hasNext()){
            Integer next = it.next();
            l.info(next.toString());
            l.info(h.get(next));
            Q.setParameter(next,h.get(next));
        }
        return Q.getResultList(); // if no result NoResultException

    }



}
