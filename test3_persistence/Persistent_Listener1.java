package test3_persistence;

import javax.inject.Inject;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import java.util.logging.Logger;

/**
 * Created by Giovanni on 10/05/2015.
 */
public class Persistent_Listener1 {

    // @Inject not working
    Logger l = Logger.getLogger("xg");


    @PostLoad
    @PostPersist
    @PostUpdate

    public void method1 (Object entity) {

        l.info("persistence listener : postload, postpersist,postupdate");

    }



}
