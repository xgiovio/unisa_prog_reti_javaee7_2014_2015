package test3_persistence;

import javax.inject.Inject;
import javax.persistence.*;
import java.util.logging.Logger;

/**
 * Created by Giovanni on 10/05/2015.
 */
public class Persistent_Listener2 {

    // @Inject not working
    Logger l = Logger.getLogger("xg");


    @PrePersist
    @PreUpdate

    public void method1 (Object entity) {

        l.info("persistence listener 2 : PrePersist, PreUpdate");

    }



}
