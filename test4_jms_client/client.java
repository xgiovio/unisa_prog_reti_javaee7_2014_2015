package test4_jms_client;

import test4_jms.GeneralMessage;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Created by Giovanni on 31/05/2015.
 */
public class client {

    public static void main(String[] args) throws NamingException {


        GeneralMessage gm = new GeneralMessage();
        gm.s = "messaggio del caiser 2";

        Context jndiContext = new InitialContext();

        ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext.lookup("jms/javaee7/ConnectionFactory");
        Destination topic = (Destination) jndiContext.lookup("jms/javaee7/Topic");
        try (JMSContext jmsContext = connectionFactory.createContext()) {
            jmsContext.createProducer().setProperty("author", 0).send(topic,gm);
        }
    }




}
