package test4_jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.logging.Logger;

/**
 * Created by Giovanni on 31/05/2015.
 */
@MessageDriven(mappedName = "jms/javaee7/Topic", activationConfig = {
        @ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "author = 0")
        })
public class MDB implements MessageListener {

    @Inject
    Logger l;

    public void onMessage(Message message) {
        try {
            GeneralMessage m = message.getBody(GeneralMessage.class);
            l.info("Messaggio: " + m.s);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
