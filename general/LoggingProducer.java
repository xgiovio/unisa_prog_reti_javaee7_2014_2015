package general;

import javax.enterprise.inject.Produces;
import java.util.logging.Logger;


public class LoggingProducer {

    @Produces
    public Logger produceLogger() {
        return Logger.getLogger("xglogger");
    }
}
